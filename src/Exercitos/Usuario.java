package Exercitos;

import Unidades.*;
import java.util.ArrayList;

/**
 *Classe que estrutura o exército do usuário. 
 * Contém ArrayList para cavaleiro, general, padre, infantaria e lanceiro, também possuia variável que quarda dinheiro e métodos para utilização.
 * @author Augusto Garcia e Éric Peralta.
 */
public class Usuario {
    private static int dinheiro = 2500;
    
    private static int cavQunt = 0;
    private static int genQunt = 0;
    private static int infQunt = 0;
    private static int lanQunt = 0;
    private static int padQunt = 0;
    
    //Unidades    

    /**
     *Método que remove um cavaleiro.
     */
    public static void rmCav(){
        cavQunt--;
    }

    /**
     *Método que remove um padre.
     */
    public static void rmPad(){
        padQunt--;
    }

    /**
     *Método que remove um lanceiro negro.
     */
    public static void rmLan(){
        lanQunt--;
    }

    /**
     *Método que remove infantaria.
     */
    public static void rmInf(){
        infQunt--;
    }

    /**
     *Método que remove um general.
     */
    public static void rmGen(){
        genQunt--;
    }

    /**
     *Método que retorna a quantiade de cavaleiros.
     * @return int - quantidade de cavaleiros.
     */
    public static int getCavQunt() {
        return cavQunt;
    }

   /**
     *Método que retorna a quantiade de generais.
     * @return int - quantidade de generais.
     */
    public static int getGenQunt() {
        return genQunt;
    }

    /**
     *Método que retorna a quantiade de infantaria.
     * @return int - quantidade de infantaria.
     */
    public static int getInfQunt() {
        return infQunt;
    }

    /**
     *Método que retorna a quantiade de lanceiros negros.
     * @return int - quantidade de lanceiros dengros.
     */
    public static int getLanQunt() {
        return lanQunt;
    }

    /**
     *Método que retorna a quantiade de padre.
     * @return int - quantidade de padre.
     */
    public static int getPadQunt() {
        return padQunt;
    }
   
    /**
     *Método que adiciona um cavaleiro.
     */
    public static void addCav(){
        Usuario.cavQunt++;
    }
    
    /**
     *Método que adiciona um general.
     */
    public static void addGen(){
        Usuario.genQunt++;
    }
    
    /**
     *Método que adiciona uma infantaria.
     */
    public static void addInf(){
        Usuario.infQunt++;
    }
    
    /**
     *Método que adiciona um lanceiro negro.
     */
    public static void addLan(){
        Usuario.lanQunt++;
    }
    
    /**
     *Método que adiciona um padre.
     */
    public static void addPad(){
        Usuario.padQunt++;
    }
    
    //Dinheiro
    /**
     *Método que retorna a quantidade de dinheiro.
     * @return int - dinheiro quardado.
     */
    public static int getDinheiro() {
        return dinheiro;
    }

    /**
     * Método que adiciona mais dinheiro.
     * @param bon int - bonus em dinheiro.
     */
    public static void Bonus(int bon){
        Usuario.dinheiro+=bon;
    }
    /**
     * Método que diminui o dinheiro quardado.
     * @param qunt int - dinheiro a ser reduzido.
     */
    public static void diminui(int qunt){
        Usuario.dinheiro-=qunt;
    }

    /**
     * Método para definir dinheiro.
     * @param dinheiro int - nova quantia de dinheiro.
     */
    public static void setDinheiro(int dinheiro) {
        Usuario.dinheiro = dinheiro;
    }
}
