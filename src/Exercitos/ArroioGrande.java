package Exercitos;

import java.util.ArrayList;
import Unidades.*;
/** 
 * Classe que estrutura o exército inimigo da batalha de Arroi Grande.
 * Contém quantidade de tropas e métodos para utilização.
 * @author Augusto Garcia e Éric Peralta.
 */
public class ArroioGrande {    
    private static int quntGen = 1;
    private static int quntCav = 5;
    private static int quntPad = 2;
    private static int quntLan = 7;
    private static int quntInf = 3;
    
    /**Método construtor de ArroioGrande*/
    ArroioGrande(){}
    /**Método para retorno da quantidade de generais.
     * @return int - quantidade de generais.
     */
    public static int getQuntGen() {
        return quntGen;
    }
    /**Método para retorno da quantidade de cavaleiros.
     * @return int - quantidade de cavaleiros.
     */
    public static int getQuntCav() {
        return quntCav;
    }
     /**Método para retorno da quantidade de padres.
     * @return int - quantidade de padres.
     */
    public static int getQuntPad() {
        return quntPad;
    }
     /**Método para retorno da quantidade de lanceiros negros.
     * @return int - quantidade de lanceiros negros.
     */
    public static int getQuntLan() {
        return quntLan;
    }
     /**Método para retorno da quantidade de infantarias.
     * @return int - quantidade de infantarias.
     */
    public static int getQuntInf() {
        return quntInf;
    }
    
    
}
