/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Unidades;

import Exercitos.Dados;

/**
 *Classe de infantaria.
 * @author Augusto Garcia e Éric Peralta 
 */
public class Infantaria extends Unidade{

    /**
     *Método construtor de infantaria.
     */
    public Infantaria(){
        this.arma = new Item(100,300);
    }
   
    /**
     *Método que executa o ataque especial.
     * @param inimigo Unidade - inimigo que receberá o dano.
     */
    @Override
   public void ataqueEspecial(Unidade inimigo){
       if(this.arma.getDefesa()>0 && r.nextInt(10) == 0){
           if(inimigo.arma.getDefesa()>0){
               inimigo.arma.setDefesa(0);
           }
           
           else{
               inimigo.setVida(0);
           }
       }
   }
   
    /**
     **Método que define vida e ataque de Infataria.
     */
    @Override
    public void setTudo(){
        this.ataque = Dados.getInfAtk();
        this.vida = Dados.getInfVid();
    }
}
