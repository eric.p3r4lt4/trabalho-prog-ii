package Unidades;
import Exercitos.Dados;

/**
 *Classe de cavalerio.
 * @author Augusto Garcia e Éric Peralta 
 */
public class Cavaleiro extends Unidade{

    /**
     *Método construtor de cavaleiro.
     */
    public Cavaleiro(){
        this.arma = new Item(50,100);
    }
    
    /**
     *Método que executa o ataque especial.
     * @param inimigo Unidade - inimigo que receberá o dano.
     */
    @Override
    public void ataqueEspecial(Unidade inimigo){
        if(this.arma.getDefesa()>0 && r.nextInt(10)<5){
            inimigo.sofreDano(this.getAtaque());
        }
    }
    
    /**
     *Método que define vida e ataque de cavaleiro.
     */
    @Override
    public void setTudo(){
        this.ataque = Dados.getCavAtk();
        this.vida = Dados.getCavVid();
    }
}