package Unidades;

import Exercitos.Dados;
import java.util.ArrayList;

/**
 * Classe de general.
 * @author Augusto Garcia e Éric Peralta
 */
public class General extends Unidade{

    /**
     *Método construtor de general.
     */
    public General(){
       this.arma = new Item(100,100);
   }
   
    /**
     *Método que executa o ataque especial.
     * @param inimigo Unidade - inimigo que receberá o dano.
     */
    @Override
   public void ataqueEspecial(Unidade inimigo){
       if(inimigo.getVida()<1 && r.nextInt(4) == 0 && this.arma.getDefesa()>0){
           this.setAtaque(this.getAtaqueBase()*2);
       }
   }
   
    /**
     *Método que define vida e ataque de general.
     */
    @Override
    public void setTudo(){
        this.ataque = Dados.getGenAtk();
        this.vida = Dados.getGenVid();
    }

}
