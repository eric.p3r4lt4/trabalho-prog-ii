
package projetogame;

import Exercitos.Usuario;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.text.Text;
import javafx.scene.control.Label;

/**
 *Classe Controller da Tela Inicial
 * @author Augusto Garcia e Éric Peralta
 */
public class TelaInicialController implements Initializable {
    @FXML
    private Label dinheiro;
    @FXML private Label cavQunt;
    @FXML private Label genQunt;
    @FXML private Label padQunt;
    @FXML private Label infQunt;
    @FXML private Label lanQunt;
    @FXML private Label avi;
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        dinheiro.setText("R$ " + Usuario.getDinheiro());
        cavQunt.setText(String.valueOf(Usuario.getCavQunt()));
        genQunt.setText(String.valueOf(Usuario.getGenQunt()));
        padQunt.setText(String.valueOf(Usuario.getPadQunt()));
        infQunt.setText(String.valueOf(Usuario.getInfQunt()));
        lanQunt.setText(String.valueOf(Usuario.getLanQunt()));
    }
    
    @FXML
    private void lanceirosButton(){
        ProjetoGame.trocaTela("lanceiros.fxml");
    }
    
    @FXML
    private void cavaleiroButton(){
        ProjetoGame.trocaTela("CavaleiroTela.fxml");
    }
    
    @FXML
    private void batalharButton(){
        if(Usuario.getCavQunt()==0&&Usuario.getPadQunt()==0&&Usuario.getGenQunt()==0&&Usuario.getInfQunt()==0&&Usuario.getLanQunt()==0)
        avi.setText("Aviso: Não há tropas!");
        else{
        ProjetoGame.trocaTela("EscolhaBatalha.fxml"); 
        }
    }
    @FXML
    private void generalButton(){
        ProjetoGame.trocaTela("General.fxml");
    }
    @FXML
    private void padreButton(){
        ProjetoGame.trocaTela("PadreTela.fxml");
    }
    @FXML
    private void infantariaButton(){
        ProjetoGame.trocaTela("InfantariaTela.fxml");
    }
}
