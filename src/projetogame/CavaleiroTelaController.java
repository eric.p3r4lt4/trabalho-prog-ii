/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projetogame;

import Exercitos.Dados;
import Exercitos.Usuario;
import Unidades.Cavaleiro;
import javafx.scene.control.TextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
/**
 * FXML Controller class
 *Classe controller da tela de cavaleiro.
 * @author Augusto Garcia e Éric Peralta
 */
public class CavaleiroTelaController implements Initializable {
    @FXML private Label dindin;
    @FXML private Label aviso;
    @FXML private TextField qunt;
    @FXML private Label cust;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        dindin.setText("R$ " + Usuario.getDinheiro());
    }
    @FXML
    private void calcust(){
        cust.setText(String.valueOf(Dados.getCavCust()*Integer.parseInt(qunt.getText())));
    }
    @FXML
    private void VoltarButton(){
        int val = Integer.parseInt(qunt.getText());
        
        if((Dados.getCavCust()*val) <= Usuario.getDinheiro()){
            Usuario.diminui(val*Dados.getCavCust());
           for(int i=0;i<val;i++){
               Usuario.addCav();
           }
            ProjetoGame.trocaTela("TelaInicial.fxml");
        }
        
        else{
            aviso.setText("Não a dinheiro suficiente");
        }
    }
    
    @FXML
    private void maisVida(){
        if(Usuario.getDinheiro()>=10){
            Dados.setCavVid(Dados.getCavVid() + 10);
            Usuario.diminui(10);
            atualizaDinheiro();
        }
    }
    
    private void atualizaDinheiro(){
        dindin.setText("R$ "+ Usuario.getDinheiro());
    }
    
    @FXML
    private void maisAtaque(){
        if(Usuario.getDinheiro()>=10){
            Dados.setCavAtk(Dados.getCavAtk() + 10);
            Usuario.diminui(10);
            atualizaDinheiro();
        }
    }
}
