package projetogame;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import Exercitos.Dados;
import Exercitos.Usuario;
import javafx.scene.control.TextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import Unidades.LanceiroNegro;

/**
 * FXML Controller class
 *Classe controller da tela de Lanceiro Negro
 * @author Augusto Garcia e Éric Peralta
 */
public class LanceirosController implements Initializable {

    @FXML private Label dindin;
    @FXML private TextField qunt;
    @FXML private Label aviso;
    @FXML private Label cust;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        dindin.setText("R$ " + Usuario.getDinheiro());
    }
    @FXML
    private void calcust(){
        cust.setText(String.valueOf(Dados.getLanCust()*Integer.parseInt(qunt.getText())));
    }
    @FXML
    private void VoltarButton(){
        int val = Integer.parseInt(qunt.getText());
        if((Dados.getLanCust()*val) <= Usuario.getDinheiro()){
            Usuario.diminui(val*Dados.getLanCust());
            for(int i=0;i<val;i++){
               Usuario.addLan();
            }
            ProjetoGame.trocaTela("TelaInicial.fxml");
        }
        
        else{
            aviso.setText("Não a dinheiro suficiente");
        }
    }
    
    @FXML
    private void maisVida(){
        if(Usuario.getDinheiro()>=10){
            Dados.setLanVid(Dados.getLanVid() + 10);
            Usuario.diminui(10);
            atualizaDinheiro();
        }
    }
    
    @FXML
    private void maisAtaque(){
        if(Usuario.getDinheiro()>=10){
            Dados.setLanAtk(Dados.getLanAtk() + 10);
            Usuario.diminui(10);
            atualizaDinheiro();
        }
    }
    
    private void atualizaDinheiro(){
        dindin.setText("R$ "+ Usuario.getDinheiro());
    }
}
